package logger

import (
	"errors"
	"io"
	"log"
	"os"
	"strconv"
	"time"
)

var logFile *os.File

/**	logger level type	*/
const (
	DEBUG = iota
	INFO
	WARN
	ERR
	PANIC
)

/**	logger level	*/
var loggerLevel = DEBUG

type bomLogger struct {
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile) // 로그에 시간정보 + 파일명 + 로그 출력 줄 번호가 표시되게끔 설정

	go checkLogger()
}

func checkLogger() {
	var ckDay = -1
	var nowTime time.Time

	for {
		nowTime = time.Now()

		if ckDay != nowTime.YearDay() {
			// 날짜가 바뀌었을 경우 처리
			changeLogger(&nowTime)    // 로그 출력 정보 변경
			ckDay = nowTime.YearDay() // 날짜 정보를 현재 날짜로 변경
		}

		time.Sleep(10 * time.Second) // 10초마다 날짜가 바뀌었는지 검사
	}
}

// 날짜가 바뀌었을 경우 로그 파일 정보 변경 처리
func changeLogger(nowTime *time.Time) {
	year, month, day := nowTime.Date()
	var date = strconv.Itoa(year) + "-" + strconv.Itoa(int(month)) + "-" + strconv.Itoa(day) // 현재 날짜
	os.Mkdir("logs", 0775)                                                                   // 로그 폴더 생성

	// 로그 파일 쓰기 정보 생성
	f, err := os.OpenFile("logs/bom-"+date+".log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)

	if err != nil {
		// 파일 연결 중 오류가 있을 경우 콘솔 출력만 설정
		log.SetOutput(os.Stdout)
	} else {
		// 파일 출력과 콘솔 출력 설정
		log.SetOutput(io.MultiWriter(f, os.Stdout))
	}

	if logFile != nil {
		// 기존 파일 출력 정보가 있을 경우 정상 종료
		logFile.Close()
	}

	logFile = f // 현재 파일 출력 정보를 변수에 추가하여 나중에 정상 종료할 수 있게 처리
}

// 로그레벨설정
func SetLoggerLevel(lv int) error {
	if lv == DEBUG || lv == INFO || lv == WARN || lv == ERR || lv == PANIC {
		loggerLevel = lv
		return nil
	} else {
		return errors.New("Logger level value fail. " + strconv.Itoa(lv))
	}
}

// 로그레벨
func LoggerLevel() int {
	return loggerLevel
}

// 로그객체생성
func New() *bomLogger {
	return &bomLogger{}
}
