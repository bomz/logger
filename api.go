package logger

import (
	"fmt"
	"log"
)

// debug level log
func (l *bomLogger) Debug(v ...interface{}) {
	logPrint(DEBUG, v)
}

// info level log
func (l *bomLogger) Info(v ...interface{}) {
	logPrint(INFO, v)
}

// warn level log
func (l *bomLogger) Warn(v ...interface{}) {
	logPrint(WARN, v)
}

// error level log
func (l *bomLogger) Err(v ...interface{}) {
	logPrint(ERR, v)
}

// panic level log
func (l *bomLogger) Panic(v ...interface{}) {
	logPrint(PANIC, v)
}

// is debug
func (l *bomLogger) IsDebug() bool {
	return loggerLevel <= DEBUG
}

// is info
func (l *bomLogger) IsInfo() bool {
	return loggerLevel <= INFO
}

// is warn
func (l *bomLogger) IsWarn() bool {
	return loggerLevel <= WARN
}

// is err
func (l *bomLogger) IsErr() bool {
	return loggerLevel <= ERR
}

// is panic
func (l *bomLogger) IsPanic() bool {
	return loggerLevel <= PANIC
}

// print log
func logPrint(c int, v ...interface{}) {
	if loggerLevel <= c {
		log.Output(3, fmt.Sprintln(v...))
	}
}
